package Page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class YelpClass {

	WebDriver driver;
	
	public YelpClass(WebDriver driver) {
		this.driver = driver;
	}
	//Link Restaurants
		By link_Restaurants = By.linkText("Restaurants");
		//
		By txt_Finder = By.id("search_description");
		//Button 
		By btn_Search = By.xpath("//body/yelp-react-root[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/form[1]/div[1]/div[2]/div[1]/div[2]/button[1]/div[1]/span[1]/span[1]");
		
		//Page
		By PagePizza = By.xpath("//body/yelp-react-root[1]/div[1]/div[4]/div[1]/div[1]/div[1]/div[2]/div[1]/ul[1]/li[21]/div[1]/div[2]/span[1]");
		By PageFilters = By.xpath("//body/yelp-react-root[1]/div[1]/div[4]/div[1]/div[1]/div[1]/div[2]/div[1]/ul[1]/li[21]/div[1]/div[2]/span[1]");
		//Filters First Record
		By Filter_GoodForKids = By.xpath("//input[@value='GoodForKids']");
		By Filter_Radio_Driving = By.xpath("//input[@value='g:-122.487602234,37.7202197691,-122.385292053,37.8016462369']");
		
		
		//Filters Second Record
		By Filter_GoodForGroups= By.xpath("//input[@value='RestaurantsGoodForGroups']");
		By Filter_Radio_Biking = By.xpath("//input[@value='g:-122.462024689,37.7405847889,-122.410869598,37.7812980238']");
														
		
		//Filters Third Record
		By Filter_HasTV = By.xpath("//input[@value='HasTV']");
		By Filter_Radio_Walking = By.xpath("//input[@value='g:-122.449235916,37.7506973336,-122.423658371,37.7710539699']");
		
		By Result = By.xpath("//div[@class=' border-color--default__09f24__R1nRO']/ul [@class=' undefined list__09f24__17TsU']/li [8]");
		
		//Information of Restaurant
		By Info_Name = By.xpath("//div [@class='lemon--div__373c0__1mboc arrange-unit__373c0__o3tjT arrange-unit-fill__373c0__3Sfw1 border-color--default__373c0__3-ifU'] / p[2]");
		By Info_Street = By.xpath("//body/div[@id='wrap']/div[3]/yelp-react-root[1]/div[1]/div[4]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/section[1]/div[1]/div[3]/div[1]/div[1]/p[1]/p[1]");
		By Info_Number = By.xpath("//body/div[@id='wrap']/div[3]/yelp-react-root[1]/div[1]/div[4]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/section[1]/div[1]/div[2]/div[1]/div[1]/p[2]");
		By Info_Stars = By.xpath("/html/body/div[2]/div[3]/yelp-react-root/div/div[3]/div[1]/div[1]/div/div/div[2]/div[1]/span/div");
		//Review
		
		By R_One = By.xpath("/html/body/div[2]/div[3]/yelp-react-root/div/div[4]/div/div/div[2]/div/div/div[1]/div/div[1]/div[2]/section[2]/div[2]/div/ul/li[1]/div/div[3]/p/span");
		By R_Two = By.xpath("/html/body/div[2]/div[3]/yelp-react-root/div/div[4]/div/div/div[2]/div/div/div[1]/div/div[1]/div[2]/section[2]/div[2]/div/ul/li[2]/div/div[4]/p/span");
		
		
		public void SelectRestaurants() throws InterruptedException {
			driver.findElement(link_Restaurants).click();
			Thread.sleep(3000);
		}

		public void InsertText() throws InterruptedException {
			System.out.println("User enters Pizza in the search engine.");
			driver.findElement(txt_Finder).sendKeys("Pizza");
			Thread.sleep(3000);
			driver.findElement(btn_Search).click();
			Thread.sleep(3000);
			System.out.println("\nIn this page the system shows 10 results.");
			System.out.print("The number of page is: ");
			String Page = driver.findElement(PagePizza).getText();
			System.out.println(Page+".");
		}
		public void FirstRecord() throws InterruptedException {
			System.out.println("User selects two filters: Good for Kids and Driving");
			driver.findElement(Filter_GoodForKids).click();
			Thread.sleep(3000);
			driver.findElement(Filter_Radio_Driving).click();
			Thread.sleep(3000);
			System.out.println("In this page the system shows 10 results.");
			System.out.print("The number of page is: ");
			String Page = driver.findElement(PageFilters).getText();
			System.out.println(Page+".");
			
		}
		
		public void SecondRecord() throws InterruptedException {
			System.out.println("User selects two filters: Good for Groups and Biking");
			driver.findElement(Filter_GoodForGroups).click();
			Thread.sleep(3000);
			driver.findElement(Filter_Radio_Biking).click();
			Thread.sleep(3000);
			System.out.println("In this page the system shows 10 results.");
			System.out.print("The number of page is: ");
			String Page = driver.findElement(PageFilters).getText();
			System.out.println(Page+".");
			
		}
		
		public void ThirdRecord() throws InterruptedException {
			System.out.println("User selects two filters: Has TV and Walking");
			driver.findElement(Filter_HasTV).click();
			Thread.sleep(3000);
			driver.findElement(Filter_Radio_Walking).click();
			Thread.sleep(3000);
			System.out.println("In this page the system shows 10 results.");
			System.out.print("The number of page is: ");
			String Page = driver.findElement(PageFilters).getText();
			System.out.println(Page+".");
		}

		public void SelectFirstOption() throws InterruptedException {
			System.out.println("User selects the first option.");
			driver.findElement(Result).click();
			Thread.sleep(3000);
		}
		
		public void Test() {
			String Name = driver.findElement(Info_Name).getText();
			System.out.println(Name);
		}
		
		public void Number() {
			String Number = driver.findElement(Info_Number).getText();
			System.out.println(Number);
		}

		public void Street() {
			String Street = driver.findElement(Info_Street).getText();
			System.out.println(Street);
		}
				
		public void Stars() throws Exception {
			
			System.out.println(""+getValue(Info_Stars,"aria-label")+"");
			
		}

		private String getValue(By element, String value) throws Exception{
			try {
				WebElement webElement = driver.findElement(element);
				return webElement.getAttribute(value);
				
			}catch (Exception ey) {
				throw new Exception ("message");
			}
		}

		public void ReviewOne() {
			String R1 = driver.findElement(R_One).getText();
			System.out.println(R1);
		}
		public void ReviewTwo() {
			String R2 = driver.findElement(R_Two).getText();
			System.out.println(R2);
		}

}
