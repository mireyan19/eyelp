package StepsDefinition;

import Page.YelpClass;
import io.cucumber.java.en.*;

public class YelpSteps extends Base{
	YelpClass yelp;
	
	@Given("User enters to Yelp website")
	public void user_enters_to_Yelp_website() {
		
		System.out.println("User enter to Website: Yelp");
	}

	@When("Clicks on Restaurants")
	public void clicks_on_Restaurants() throws InterruptedException {
		
	yelp = new YelpClass(driver);
			
	yelp.SelectRestaurants();	
	}

	@And("Enters Pizza in the search engine")
	public void enters_Pizza_in_the_search_engine() throws InterruptedException {
		yelp.InsertText();
	}

	@And("Selects First")
	public void selects_First() throws InterruptedException {
		yelp.FirstRecord();
	}
	
	@When("Selects Second")
	public void selects_Second() throws InterruptedException {
		
		yelp.SecondRecord();
	}

	@When("Selects Third")
	public void selects_Third() throws InterruptedException {
		yelp.ThirdRecord();
	}
	@When("Selects the first option")
	public void selects_the_first_option() throws InterruptedException {
		yelp.SelectFirstOption();
	}
	@Then("The information must be displayed")
	public void the_information_must_be_displayed() throws Exception {
		System.out.println("\nThe information is displayed");
		System.out.println("--------------------");
		System.out.println("The website is:");
		yelp.Test();
		System.out.println("--------------------");
		System.out.println("The number is:");
		yelp.Number();
		System.out.println("--------------------");
		System.out.println("The address is:");
		yelp.Street();
		System.out.println("\n--------------------");
		System.out.println("Star Rating:");
		yelp.Stars();
		System.out.println("\n--------------------");
		System.out.println("\nFirst Review: ");
		yelp.ReviewOne();
		System.out.println("\n--------------------");
		System.out.println("\nSecond Review: ");
		yelp.ReviewTwo();
		System.out.println("\n--------------------");
	}

}
