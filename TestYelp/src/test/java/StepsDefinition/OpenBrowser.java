package StepsDefinition;

import org.openqa.selenium.firefox.FirefoxDriver;

import io.cucumber.java.*;

public class OpenBrowser {
	public static FirefoxDriver driver;

	public static FirefoxDriver getDriver() {

		return driver;		
	}

	@Before
	public void setUp() {
		System.setProperty("webdriver.gecko.driver","/Users/mirellayanac/Desktop/ProjectMirellaThird/TestYelp/src/test/resources/driver/geckodriver");

		driver = new FirefoxDriver();
		driver.navigate().to("https://www.yelp.com/");
		driver.manage().window().maximize();
	}

	@After
	public void tearDown() {

		driver.quit();
	}
}
