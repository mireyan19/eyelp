package StepsDefinition;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features="/Users/mirellayanac/Desktop/ProjectMirellaThird/TestYelp/src/test/resources/Feature/Yelp.feature",
glue = {"StepsDefinition"},
monochrome = true)

public class TestRunner {

}
